use crate::instruction::Opcode;

pub struct VM {
    /// 寄存器数据
    pub registers: [i32; 32],
    /// 程序计数器
    pc: usize,
    /// 指令集
    pub program: Vec<u8>,
    /// 堆
    heap: Vec<u8>,
    /// 除法结果小数余数
    remainder: usize,
    /// 最新的比较结果
    equal_flag: bool,
}

impl VM {
    pub fn new() -> VM {
        VM {
            registers: [0; 32],
            program: vec![],
            heap: vec![],
            pc: 0,
            remainder: 0,
            equal_flag: false,
        }
    }

    pub fn add_byte(&mut self, byte: u8) {
        self.program.push(byte);
    }

    pub fn run(&mut self) {
        let mut is_done = false;
        while !is_done {
            is_done = self.execute_instruction();
        }
    }

    pub fn run_once(&mut self) {
        self.execute_instruction();
    }

    fn execute_instruction(&mut self) -> bool {
        if self.pc >= self.program.len() {
            return true;
        }
        match self.decode_opcode() {
            Opcode::LOAD => {
                let register = self.next_8_bits() as usize;
                let number = self.next_16_bits() as u32;
                self.registers[register] = number as i32;
            }
            Opcode::HLT => {
                println!("HLT encountered");
                return true;
            }
            Opcode::ADD => {
                let rg1 = self.registers[self.next_8_bits() as usize];
                let rg2 = self.registers[self.next_8_bits() as usize];
                self.registers[self.next_8_bits() as usize] = rg1 + rg2;
            }
            Opcode::SUB => {
                let rg1 = self.registers[self.next_8_bits() as usize];
                let rg2 = self.registers[self.next_8_bits() as usize];
                self.registers[self.next_8_bits() as usize] = rg1 - rg2;
            }
            Opcode::MUL => {
                let rg1 = self.registers[self.next_8_bits() as usize];
                let rg2 = self.registers[self.next_8_bits() as usize];
                self.registers[self.next_8_bits() as usize] = rg1 * rg2;
            }
            Opcode::DIV => {
                let rg1 = self.registers[self.next_8_bits() as usize];
                let rg2 = self.registers[self.next_8_bits() as usize];
                self.registers[self.next_8_bits() as usize] = rg1 / rg2;
                self.remainder = (rg1 % rg2) as usize;
            }
            Opcode::JMP => {
                let target = self.registers[self.next_8_bits() as usize];
                self.pc = target as usize;
            }
            Opcode::JMPF => {
                let step = self.registers[self.next_8_bits() as usize] as usize;
                self.pc += step;
            }
            Opcode::JMPB => {
                let step = self.registers[self.next_8_bits() as usize] as usize;
                self.pc -= step;
            }
            Opcode::EQ => {
                let rg1 = self.registers[self.next_8_bits() as usize];
                let rg2 = self.registers[self.next_8_bits() as usize];
                if rg1 == rg2 {
                    self.equal_flag = true;
                } else {
                    self.equal_flag = false;
                }
                self.next_8_bits();
            }
            Opcode::NEQ => {
                let rg1 = self.registers[self.next_8_bits() as usize];
                let rg2 = self.registers[self.next_8_bits() as usize];
                if rg1 == rg2 {
                    self.equal_flag = false;
                } else {
                    self.equal_flag = true;
                }
                self.next_8_bits();
            }
            Opcode::GT => {
                let rg1 = self.registers[self.next_8_bits() as usize];
                let rg2 = self.registers[self.next_8_bits() as usize];
                if rg1 > rg2 {
                    self.equal_flag = true;
                } else {
                    self.equal_flag = false;
                }
                self.next_8_bits();
            }
            Opcode::GTE => {
                let rg1 = self.registers[self.next_8_bits() as usize];
                let rg2 = self.registers[self.next_8_bits() as usize];
                if rg1 >= rg2 {
                    self.equal_flag = true;
                } else {
                    self.equal_flag = false;
                }
                self.next_8_bits();
            }
            Opcode::LT => {
                let rg1 = self.registers[self.next_8_bits() as usize];
                let rg2 = self.registers[self.next_8_bits() as usize];
                if rg1 < rg2 {
                    self.equal_flag = true;
                } else {
                    self.equal_flag = false;
                }
                self.next_8_bits();
            }
            Opcode::LTE => {
                let rg1 = self.registers[self.next_8_bits() as usize];
                let rg2 = self.registers[self.next_8_bits() as usize];
                if rg1 <= rg2 {
                    self.equal_flag = true;
                } else {
                    self.equal_flag = false;
                }
                self.next_8_bits();
            }
            Opcode::JMPE => {
                let rg = self.next_8_bits() as usize;
                let targe = self.registers[rg];
                if self.equal_flag {
                    self.pc = targe as usize;
                }
            }
            Opcode::JMPN => {
                let rg = self.next_8_bits() as usize;
                let targe = self.registers[rg];
                if !self.equal_flag {
                    self.pc = targe as usize;
                }
            }
            Opcode::ALOC => {
                let register = self.next_8_bits() as usize;
                let bytes = self.registers[register];
                let new_end = self.heap.len() as i32 + bytes;
                self.heap.resize(new_end as usize, 0);
            }
            _ => {
                println!("Unrecognized opcode found! Terminating!");
                return true;
            }
        };
        false
    }

    fn decode_opcode(&mut self) -> Opcode {
        let opcode = Opcode::from(self.program[self.pc]);
        self.pc += 1;
        opcode
    }

    fn next_8_bits(&mut self) -> u8 {
        let result = self.program[self.pc];
        self.pc += 1;
        result
    }

    fn next_16_bits(&mut self) -> u16 {
        let result = ((self.program[self.pc] as u16) << 8) | self.program[self.pc + 1] as u16;
        self.pc += 2;
        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn creat_vm_test() {
        let test_vm: VM = VM::new();
        assert_eq!(test_vm.registers[0], 0)
    }

    #[test]
    fn opcode_hlt_test() {
        let mut test_vm = VM::new();
        let test_bytes = vec![5, 0, 0, 0];
        test_vm.program = test_bytes;
        test_vm.run_once();
        assert_eq!(test_vm.pc, 1);
    }

    #[test]
    fn opcode_ig_test() {
        let mut test_vm = VM::new();
        let test_bytes = vec![200, 0, 0, 0];
        test_vm.program = test_bytes;
        test_vm.run_once();
        assert_eq!(test_vm.pc, 1);
    }

    #[test]
    fn load_opcode_test() {
        let mut test_vm = VM::new();
        test_vm.program = vec![0, 0, 1, 244];
        test_vm.run_once();
        println!("re: {}", test_vm.registers[0]);
        assert_eq!(test_vm.registers[0], 500);
    }

    #[test]
    fn jum_opcode_test() {
        let mut test_vm = VM::new();
        test_vm.registers[0] = 1;
        test_vm.program = vec![6, 0, 1, 244];
        test_vm.run_once();
        println!("re: {}", test_vm.registers[0]);
        assert_eq!(test_vm.pc, 1);
    }

    #[test]
    fn add_opcode_test() {
        let mut test_vm = VM::new();
        test_vm.program = vec![0, 0, 1, 244, 0, 1, 0, 222, 1, 0, 1, 2];
        test_vm.run();
        assert_eq!(test_vm.registers[0], 500);
        assert_eq!(test_vm.registers[1], 222);
        assert_eq!(test_vm.registers[2], 722);
    }

    #[test]
    fn jumf_opcode_test() {
        let mut test_vm = VM::new();
        test_vm.registers[0] = 2;
        test_vm.program = vec![7, 0, 0, 0, 6, 0, 0, 0];
        test_vm.run_once();
        assert_eq!(test_vm.pc, 4);
    }

    #[test]
    fn eq_opcode_test() {
        let mut test_vm = VM::new();
        test_vm.registers[0] = 10;
        test_vm.registers[1] = 10;
        test_vm.program = vec![9, 0, 1, 0, 9, 0, 1, 0];
        test_vm.run_once();
        assert_eq!(test_vm.equal_flag, true);
        test_vm.registers[1] = 20;
        test_vm.run_once();
        assert_eq!(test_vm.equal_flag, false);
    }

    #[test]
    fn jume_opcode_test() {
        let mut test_vm = VM::new();
        test_vm.registers[0] = 7;
        test_vm.equal_flag = true;
        test_vm.program = vec![15, 0, 1, 0, 9, 0, 1, 0, 0, 0, 1, 22];
        test_vm.run_once();
        assert_eq!(test_vm.pc, 7);
    }

    #[test]
    fn aloc_opcode_test() {
        let mut test_vm = VM::new();
        test_vm.registers[0] = 1024;
        test_vm.program = vec![17, 0, 0, 0];
        test_vm.run_once();
        assert_eq!(test_vm.heap.len(), 1024);
    }
}
